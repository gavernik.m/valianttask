const express = require('express');
const mongodb = require('mongodb');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwtHelper = require('../../helpers/jwt.helper')

const mongoURL = require('../../config').mongoURL
console.log(mongoURL)



// get users
router.get('/', async (req, res) => {
    res.send({ status: 'up and running' })
});

// register user
router.post('/register', async (req, res) => {
    const payload = {
        ...req.body,
        createAt: new Date()
    };
    const users = await loadUserCollection();
    generateHash(req.body.password).then(
        hash => {
            payload.password = hash
            users.insertOne(payload, function (err, result) {
                if (err) {
                    res.status(400)
                }
                res.status(201).send(result);
            });
        })
})

// login user
router.post('/login', async (req, res) => {
    const login = req.body;
    const members = await loadUserCollection();



    members.findOne({ "username": login.username }, function (err, user) {



        if (err) { res.status(401).send(err) }

        if (user === null) {
            res
                .json({
                    'isLogin': false,
                    'msg': 'Login failed. Incorect credentials. NOT FOUND'
                })
        } else {
            validPassword(login.password, user.password)
                .then(valid => {
                    let result
                    if (!valid) {
                        result = {
                            isLogin: valid,
                            msg: 'Login ' + (valid ? 'successful' : 'failed. Incorect credentials. ')
                        }
                    } else {
                        result = {
                            isLogin: valid,
                            msg: 'Login ' + (valid ? 'successful' : 'failed. Incorect credentials. '),
                            userType: user.userType,
                            fullname: `${user.fn} ${user.ln}`,
                            authToken: jwtHelper.generateToken('auth', user),
                            refreshToken: jwtHelper.generateToken('refresh', user),
                            _id: user._id
                        }
                    }
                    res.send(result)
                })
                .catch(e => console.log(e))
        }
    })
})


var validPassword = function (password, passwordHash) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, passwordHash, function (err, res) {
            if (err) { return reject(err) }
            return resolve(res);
        });
    })
}

var generateHash = function (password) {
    const saltRounds = 10;
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(saltRounds, function (err, salt) {
            bcrypt.hash(password, salt, function (err, hash) {
                if (err) { return reject(err) }
                return resolve(hash);
            });
        });
    });
};

const loadUserCollection = async () => {
    const client = await mongodb.MongoClient.connect(mongoURL, {
        useNewUrlParser: true
    })
    return client.db('vue_express').collection('users');
}

module.exports = router;