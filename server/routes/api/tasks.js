const express = require('express');
const mongodb = require('mongodb');
const router = express.Router();

const mongoURL = require('../../config').mongoURL

// get posts
router.get('/', async (req, res) => {
    const tasks = await loadTaskCollection();
    res.send(await tasks.find({}).sort({ createdAt: -1 }).toArray());

});

// Add post
router.post('/', async (req, res) => {

    const posts = await loadTaskCollection();
    await posts.insertOne({
        ...req.body,
        createdAt: new Date()
    })
    res.status(201).send();
})

// Update post
router.put('/:id', async (req, res) => {

    const posts = await loadTaskCollection();
    const payload = req.body;

    await posts.updateOne(
        { _id: new mongodb.ObjectID(req.params.id) },
        { $set: { name: payload.name, description: payload.description, completed: payload.completed } },
        { upsert: true });
    res.status(200).send();
})

// Dellete post
router.delete('/:id', async (req, res) => {

    const posts = await loadTaskCollection();
    await posts.deleteOne({ _id: new mongodb.ObjectID(req.params.id) })

    res.status(200).send();
})

const loadTaskCollection = async () => {
    const client = await mongodb.MongoClient.connect(mongoURL, {
        useNewUrlParser: true
    })
    return client.db('vue_express').collection('tasks');
}

module.exports = router;