const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');


const jwtHelper = require('./helpers/jwt.helper.js');

jwtHelper.authenticate()

const app = express();

// Middleware
app.use(bodyParser.json());
app.use(cors());

const users = require('./routes/api/users');
const tasks = require('./routes/api/tasks');

const logRequestStart = (req, res, next) => {
  console.info(`${req.method} ${req.originalUrl}`)
  res.on('finish', () => {
    console.info(`${res.statusCode} ${res.statusMessage}; ${res.get('Content-Length') || 0}b sent`)
  })
  next()
}

const checkingAccess = async (req, res, next) => {

  let token = req.headers.authorization;
  if (token !== undefined) {
    if (token.startsWith('Bearer ')) {
      // Remove Bearer from string
      token = token.slice(7, token.length);
    }
    if (token) {
      let tCheck = await jwtHelper.authenticate(token);
      if (tCheck.status === 'TokenValid') {
        // enablePrivateRoutes();
        return next();

      } else {
        res.status(401).json({
          success: false,
          message: 'Token is not valid'
        });
      }
    } else {
      res.status(401).json({
        success: false,
        message: 'Auth token is not supplied'
      });
    }
  } else {
    res.status(401).json({
      success: false,
      message: 'Auth token is not supplied'
    });
  }
}
app.use(logRequestStart);
app.use('/api/users', users);

// public routes without validation

// mount all private routes
app.use('/api/tasks', checkingAccess, tasks);

// Handle production
if (process.env.NODE_ENV === 'production') {
  // Static folder
  app.use(express.static(__dirname + '/public/'));
  // Handle SPA [single page application]
  app.get(/.*/, (req, res) => res.sendFile(__dirname + '/public/index.html'));
}

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));