const prodMongo = "mongodb+srv://it:it123@cluster0-liozo.mongodb.net/test?retryWrites=true"
const devMongo = "mongodb://localhost:27017"

exports.mongoURL = process.env.NODE_ENV === 'production' ? prodMongo : devMongo;