var jwt = require('jsonwebtoken');
const SECRET_PHRASE = 'I Love Jameson';

const Authorize = async (user) => {

    var res = jwt.sign({
        exp: Math.floor(Date.now() / 1000) + (60),
        data: ''
    }, SECRET_PHRASE);
    return res

}
exports.authorize = Authorize;

const Authenticate = async (token) => {
    return jwt.verify(
        token,
        SECRET_PHRASE,
        (err, decoded) => {
            let result = {
                status: 'TokenValid',
                decoded: jwt.decode(token)
            }

            if (err) {
                result.status = err.name;
            }
            return result;
        })
}
exports.authenticate = Authenticate;

const GenerateToken = (type, user) => {
    let expiry, exp;
    switch (type) {
        case 'refresh':
            expiry = { expiresIn: 60 * 60 * 24 }
            // exp = Math.floor(Date.now() / 1000) + (60 * 60)
            break;
        case 'auth':
            expiry = { expiresIn: 60 * 60 * 3 }
            // exp = Math.floor(Date.now() / 1000) + (60)
            break;
        default:
            break;
    }
    data = {
        _id: user._id,
        firstNames: user.firstNames,
        lastName: user.lastName,
    }


    let token = jwt.sign(
        {
            data: data,
            // exp: exp,
        },
        SECRET_PHRASE,
        expiry);
    return token;
}
exports.generateToken = GenerateToken;

