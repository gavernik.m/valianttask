import { expect,  } from 'chai';
import { mount } from '@vue/test-utils';
import TableComponent from '@/components/TableComponent';

describe('TableComponent.vue', () => {

    it('Check if function is emiting', () => {
        const wrapper = mount(TableComponent, {
        });

        const type = 'TEST_TYPE';
        const payload = {
            _id: '5caff5bb61b2b4172c8970a1',
            name: 'test',
            description: 'test',
            CreatedAt: '2019-04-15T04:03:14.230Z',
            completed: false
        };

        wrapper.vm.callParent(type, payload);
        const result = wrapper.emitted('interface')
        expect(result).to.not.equal(undefined);
    });

    it('Check if function returns type property', () => {
        const wrapper = mount(TableComponent, {
        });

        const type = 'TEST_TYPE';
        const payload = {
            _id: '5caff5bb61b2b4172c8970a1',
            name: 'test',
            description: 'test',
            CreatedAt: '2019-04-15T04:03:14.230Z',
            completed: false,
        };

        wrapper.vm.callParent(type, payload);

        expect(wrapper.emitted('interface')[0][0]).to.have.property('type')
    });
});