import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import RegisterComponent from '@/components/RegisterComponent';

describe('RegisterComponent.vue', () => {
    it('Message should be empty', () => {
        const wrapper = shallowMount(RegisterComponent);
        expect(wrapper.vm.message).to.equal('');
    });

    it('Username and passwords should be empty', () => {
        const wrapper = shallowMount(RegisterComponent, {
        });
        expect(wrapper.vm.form.rFn).to.equal(null);
        expect(wrapper.vm.form.rLn).to.equal(null);
        expect(wrapper.vm.form.rUsername).to.equal(null);
        expect(wrapper.vm.form.rPassword).to.equal(null);
    });

    it('Function clear form', () => {
        const wrapper = shallowMount(RegisterComponent, {
        });

        wrapper.vm.form.rUsername = 'test';
        wrapper.vm.form.rPassword = 'test';
        wrapper.vm.form.rFn = 'test';
        wrapper.vm.form.rLn = 'test';

        wrapper.vm.clearForm();

        expect(wrapper.vm.form.rFn).to.equal(null);
        expect(wrapper.vm.form.rLn).to.equal(null);
        expect(wrapper.vm.form.rUsername).to.equal(null);
        expect(wrapper.vm.form.rPassword).to.equal(null);
    });

    it('Form invalid if not inserted data', () => {
        const wrapper = shallowMount(RegisterComponent, {
        });
        expect(wrapper.vm.$v.$invalid).to.equal(true);
    });

    it('Form valid if data', () => {
        const wrapper = shallowMount(RegisterComponent, {
        });

        wrapper.vm.form.rFn = 'test';
        wrapper.vm.form.rLn = 'test';
        wrapper.vm.form.rUsername = 'test';
        wrapper.vm.form.rPassword = 'test';
        expect(wrapper.vm.$v.$invalid).to.equal(false);
    });

});