import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import LoginComponent from '@/components/LoginComponent';

describe('LoginComponent.vue', () => {
    it('Message should be empty', () => {
        const wrapper = shallowMount(LoginComponent);
        expect(wrapper.vm.message).to.equal('');
    });

    it('Username and passwords should be empty', () => {
        const wrapper = shallowMount(LoginComponent, {
        });
        expect(wrapper.vm.form.username).to.equal(null);
        expect(wrapper.vm.form.password).to.equal(null);
    });

    it('Function clear form', () => {
        const wrapper = shallowMount(LoginComponent, {
        });

        wrapper.vm.username = 'test';
        wrapper.vm.username = 'test';

        wrapper.vm.clearForm();

        expect(wrapper.vm.form.username).to.equal(null);
        expect(wrapper.vm.form.password).to.equal(null);
    });

    it('Form invalid if not inserted data', () => {
        const wrapper = shallowMount(LoginComponent, {
        });
        expect(wrapper.vm.$v.$invalid).to.equal(true);
    });

    it('Form valid if data', () => {
        const wrapper = shallowMount(LoginComponent, {
        });

        wrapper.vm.form.username = 'mick';
        wrapper.vm.form.password = 'mick';
        expect(wrapper.vm.$v.$invalid).to.equal(false);
    });

});