// https://docs.cypress.io/api/introduction/api.html

describe('TS1', () => {
  it('TS1 - login failed - not found', () => {
    cy.visit('/');
    cy.contains('h3', 'Login');
    cy.url()
      .should('include', '/login');

    cy.log('submitting form without username and password') // if you really need this

    cy.get('#loginForm').submit();

    cy.log('filling out first name'); // if you really need this
    cy.get('#username').type('Johnny');

    cy.log('filling out last name'); // if you really need this
    cy.get('#password').type('Appleseed');

    cy.log('submitting form'); // if you really need this
    cy.get('#loginForm').submit();

    cy.get('div.msg-login').should('be.visible');
    cy.get('div.msg-login').should('be', 'Login failed. Incorect credentials.')

  });
});
describe('TS2', () => {
  it('TS2 - login failed incorrect password', () => {
    cy.visit('/');
    cy.contains('h3', 'Login');
    cy.url()
      .should('include', '/login');

    cy.log('submitting form without username and password') // if you really need this

    cy.get('#loginForm').submit();

    cy.log('filling out first name'); // if you really need this
    cy.get('#username').type('mick');

    cy.log('filling out last name'); // if you really need this
    cy.get('#password').type('Appleseed');

    cy.log('submitting form'); // if you really need this
    cy.get('#loginForm').submit();

    cy.get('div.msg-login').should('be.visible');
    cy.get('div.msg-login').should('be', 'Login failed. Incorect credentials. NOT FOUND')

  });

});

describe('TS3', () => {
  it('TS3 - login Success, Add Task, Edit Task, Mark Completed, Delete, Logout', () => {
    cy.visit('/');
    cy.log('filling out first name') // if you really need this
    cy.get('#username').type('mick')

    cy.log('filling out last name') // if you really need this
    cy.get('#password').type('mick')

    cy.log('submitting form') // if you really need this
    cy.get('#loginForm').submit();

    cy.url()
      .should('not.include', 'login')

    cy.get('#user-name-label').should('be', 'michal Gavernik')


    cy.get('#add-task-btn').click();
    cy.get('#task-name').type('test name');
    cy.get('#task-description').type('test description');

    cy.get('#save-task-btn').click();

    cy.get('#edit-task-btn').click();
    cy.get('#task-description').type(' editing')
    cy.get('#save-task-btn').click();
    cy.get('#check-task-btn').click();
    cy.get('#delete-task-btn').click()

    cy.get('#logout-btn').click()


  })



});
