/* eslint-disable indent */
/* eslint-disable linebreak-style */
import axios from 'axios';
import router from '../router';
import { TokenService } from './storage.service';

const taskUrl = 'api/tasks/';
const userUrl = 'api/users/';

class ApiService {

    isLogin;

    static init(baseURL) {
        axios.defaults.baseURL = baseURL;
    }

    static setHeader() {
        if (TokenService.getToken()) {
            axios.defaults.headers.common.Authorization = `Bearer ${TokenService.getToken()}`
        }
    }

    static removeHeader() {
        axios.defaults.headers.common = {}
    }

    // Get Task
    static getTasks() {
        this.setHeader();
        return new Promise(async (resolve, reject) => {
            try {
                const res = await axios.get(taskUrl);
                resolve(res.data);
            } catch (error) {
                reject(error);
            }
        });
    }

    // create Task
    static createTask(obj) {
        this.setHeader();
        return axios.post(taskUrl, obj);
    }

    // update Task
    static updateTask(obj) {
        this.setHeader();
        return axios.put(`${taskUrl}${obj._id}`, obj);
    }

    // delete Task
    static deleteTask(id) {
        this.setHeader();
        return axios.delete(`${taskUrl}${id} `);
    }

    // create Task
    static register(obj) {
        return axios.post(`${userUrl}register`, obj)
    }

    static login(obj) {
        return new Promise((resolve, reject) => {

            // Operating only with specific property of JSON
            axios.post(`${userUrl}login`, obj)
                .then(({ data }) => {

                    if (data.isLogin === false || data === undefined) {
                        resolve(data);
                    } else {
                        this.isLogin = data.isLogin;
                        TokenService.saveToken(data.authToken);
                        TokenService.saveRefreshToken(data.refreshToken);
                        TokenService.saveUser(data);
                        this.setHeader();
                        router.push('/');
                        resolve(data);
                    }
                }).catch((e) => {
                    reject(e);
                });
        });
    }

    static logout() {
        TokenService.logout();
        this.removeHeader();
        router.push('/login');
    }
}

export default ApiService;
