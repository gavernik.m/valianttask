import Vue from 'vue';
import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css';
import App from './App.vue';
import router from './router';
import ApiService from './services/ApiService';
import { TokenService } from './services/storage.service';




Vue.use(VueMaterial);

Vue.config.productionTip = false;

// Set the base URL of the API
ApiService.init(process.env.VUE_APP_ROOT_API);

// If token exists set header
if (TokenService.getToken()) {
  ApiService.setHeader();
}

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
